package org.myazure.image;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.myazure.api.ImageAPI;
import org.myazure.utils.F;

public class ImageImpl implements ImageAPI {

	@Override
	public String get128Code(String url) {
		String filename = UUID.randomUUID().toString();
		try {
			F.downloadImageToTmp(url, filename.toString());
		} catch (IOException e) {
			System.out.println("22222222222222");
			e.printStackTrace();
		}
		String resaultString=QrCode.decodeQr("C:\\\\imageTmp\\"+filename+".png");
		System.out.println(resaultString);
		return resaultString;
	}

	@Override
	public String getEAN13Code(String url) {
		String filename = UUID.randomUUID().toString();
		try {
			F.downloadImageToTmp(url, filename.toString());
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		String resaultString=QrCode.decodeQr("C:\\\\imageTmp\\"+filename+".png");
		if (resaultString.length()==13) {
			return resaultString;
		}
		return "";
	}

	@Override
	public void saveToTmp(String url) {

	}

	@Override
	public String getEAN13Code(File fileName) {
		String resaultString=QrCode.decodeQr(fileName.getPath());
		if (resaultString.length()==13) {
			return resaultString;
		}
		return "";
	}

}
