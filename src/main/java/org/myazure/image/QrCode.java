package org.myazure.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Random;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Reader;
import com.google.zxing.oned.EAN13Reader;

public class QrCode {
	public static String decodeQr(String filePath) {
		String retStr = "";
		if ("".equalsIgnoreCase(filePath) && filePath.length() == 0) {
			return "#FileNotFounded!#";
		}
			BufferedImage bufferedImage = null;
			try {
				bufferedImage = ImageIO.read(new FileInputStream(
						filePath));
			} catch (IOException e1) {
//				e1.printStackTrace();
				return "";
			}
			LuminanceSource source = new BufferedImageLuminanceSource(
					bufferedImage);
			Binarizer binarizer = new HybridBinarizer(source);
			BinaryBitmap bitmap = new BinaryBitmap(binarizer);
			Hashtable<DecodeHintType, Object> hintTypeObjectHashMap = new Hashtable<DecodeHintType, Object>();
			hintTypeObjectHashMap.put(DecodeHintType.CHARACTER_SET, "UTF-8");
			Result result = null;
			try {
				result = new MultiFormatReader().decode(bitmap,
						hintTypeObjectHashMap);
			} catch (NotFoundException e) {
//				e.printStackTrace();
				return "";
			}
			retStr = result.getText();
		return retStr;
	}
	
	
	
	   public static String createQrcode(String _text){
	        String qrcodeFilePath = "";
	        try {
	            int qrcodeWidth = 300;
	            int qrcodeHeight = 300;
	            String qrcodeFormat = "png";
	            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
	            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
	            BitMatrix bitMatrix = new MultiFormatWriter().encode(_text, BarcodeFormat.QR_CODE, qrcodeWidth, qrcodeHeight, hints);
	            BufferedImage image = new BufferedImage(qrcodeWidth, qrcodeHeight, BufferedImage.TYPE_INT_RGB);
	            Random random = new Random();
	            File QrcodeFile = new File("M:\\qrcode\\" + random.nextInt() + "." + qrcodeFormat);
	            ImageIO.write(image, qrcodeFormat, QrcodeFile);
	            MatrixToImageWriter.writeToFile(bitMatrix, qrcodeFormat, QrcodeFile);
	            qrcodeFilePath = QrcodeFile.getAbsolutePath();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return qrcodeFilePath;
	    }
}
