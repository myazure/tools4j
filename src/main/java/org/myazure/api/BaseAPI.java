package org.myazure.api;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;

public abstract class BaseAPI {
	protected static final String BASE_URI = "https://tools.myazure.org";
	protected static final String MEDIA_URI = "http://file.tools.myazure.org";
	protected static final String OPEN_URI = "https://open.tools.myazure.org";
	
	protected static Header jsonHeader = new BasicHeader(HttpHeaders.CONTENT_TYPE,ContentType.APPLICATION_JSON.toString());
	protected static Header xmlHeader = new BasicHeader(HttpHeaders.CONTENT_TYPE,ContentType.APPLICATION_XML.toString());
	
	protected static final String PARAM_ACCESS_TOKEN = "access_token";
	protected static final String PARAM_ACCESS_KEY = "access_token";
	
	
	
	
	
	

}
