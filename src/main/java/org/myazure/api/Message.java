package org.myazure.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qq.weixin.mp.aes.WXBizMsgCrypt;

import weixin.popular.bean.component.ComponentReceiveXML;
import weixin.popular.bean.message.EventMessage;

public interface Message {
	
	public void sentWeixinTextMsg(String appId,String toUser,String msg);
	public void sentWeixinTextMsg(String appId,String toUser,String msg,String[] args);
	public void sentWeixinMp3Msg(String appId,String toUser,String mp3Url,String[] args);
	public void sentMailMsg(String sender,String reciver,String subject,List<String> context,String[] args);
	public ComponentReceiveXML getComponentReceiveXML(HttpServletRequest request );
	public EventMessage getEventMessage (HttpServletRequest request);
	public <T> T getEventMessage(HttpServletRequest request, Class<T> clazz);
	public <T> T getEventMessage(HttpServletRequest request, HttpServletResponse response, Class<T> clazz, WXBizMsgCrypt wxBizMsgCrypt, String encodeToken);
	
}
