package org.myazure.api;

import java.io.OutputStream;

public interface IO {
	public boolean outputStreamWrite(OutputStream outputStream, String text) ;
}
