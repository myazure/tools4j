package org.myazure.api;

import java.io.File;


public interface ImageAPI    {
	public String get128Code(String url);
	public String getEAN13Code(String url);
	public String getEAN13Code(File fileName);
	public void saveToTmp(String url);
}
