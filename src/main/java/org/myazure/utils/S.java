package org.myazure.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;




import org.myazure.api.IO;
import org.myazure.response.StatusResponse;

import com.alibaba.fastjson.JSON;

public final class S {
	public static final String UTF8 = "utf-8";
	public static final String GBK = "gbk";
	public static final String GBK2312 = "gbk2312";
	public static final String ISO = "iso-8859-1";

	private S() {
		
	}

	// ä¸­å½äºº UTF8------UTF8>ISO
	// 涓浗浜� UTF8------UTF8>GBK
	// 中国�? GBK-------GBK>UTF8
	/**
	 * 
	 * @param utf8String
	 * @return
	 */
	public static final String utf8ToGBK(String utf8String) {
		try {
			return new String(
					new String(utf8String.getBytes(UTF8), ISO).getBytes(ISO),
					GBK);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return new String("");
	}

	/**
	 * 
	 * @param gbkString
	 * @return
	 */
	public static final String gbkToUtf8(String gbkString) {
		try {
			return new String(
					new String(gbkString.getBytes(GBK), ISO).getBytes(ISO),
					UTF8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return new String("");
	}

	public static final String bytes2IntNums(byte[] datas, String split) {
		StringBuffer value = new StringBuffer();
		for (int i = 0; i < datas.length; i += 2) {
			byte high = datas[i];
			byte low = datas[i + 1];
			value.append(String.valueOf(((high << 8)) | (low & 0xff))).append(
					split);
		}
		value.deleteCharAt(datas.length - 1);
		return value.toString();
	}

	public static final BigDecimal toBigDecimal(String data) {
		BigDecimal bd = new BigDecimal(data);
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return bd;
	}

	public static final Short toShort(String data) {
		return new Short(data);
	}
	

	public  static void entResponse(HttpServletResponse response, Object object) {
		try {
			response.setContentType("application/json");
			response.getWriter().write(JSON.toJSONString(object));
			response.getWriter().close();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public  static void entResponse(HttpServletResponse response, String msg) {
		try {
			org.myazure.response.StatusResponse statusResponse = new StatusResponse(msg, 0, true);
			response.setContentType("application/json");
			response.getWriter().write(JSON.toJSONString(statusResponse));
			response.getWriter().close();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static String getTimeNow(){
		return F.dateFormat().format(new Date());
	}
	public static String getTimeThen(Long time){
		return F.dateFormat().format(new Date(time));
	}
	public static String getTodayDate(){
		return F.todayFormat().format(new Date());
	}
}
