package org.myazure.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;

public final class F {
	public List<File> getFolders(String path) {
		List<File> fileList = new ArrayList<File>();
		File file = new File(path);
		if (file.exists()) {
			File[] files = file.listFiles();
			if (files.length == 0) {
				// System.out.println("文件夹是空的!");
			} else {
				for (File file2 : files) {
					if (file2.isDirectory()) {
						// System.out.println("文件夹:" + file2.getAbsolutePath());
						fileList.add(file2);
						getFolders(file2.getAbsolutePath());
					} else {
						// System.out.println("文件:" + file2.getAbsolutePath());
					}
				}
			}
		} else {
			// System.out.println("文件不存在!");
		}
		return fileList;
	}

	public static List<File> getFileList(String strPath) {
		List<File> filelist = new ArrayList<File>();
		File dir = new File(strPath);
		File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) { // 判断是文件还是文件夹
					getFileList(files[i].getAbsolutePath()); // 获取文件绝对路径
				} else if (fileName.endsWith("avi")) { // 判断文件名是否以.avi结尾
					// String strFileName = files[i].getAbsolutePath();
					// System.out.println("---" + strFileName);
					filelist.add(files[i]);
				} else {
					continue;
				}
			}
		}
		return filelist;
	}
	
	
	
	public static List<String> getFileListString(String strPath) {
		List<String> filelist = new ArrayList<String>();
		File dir = new File(strPath);
		File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) { // 判断是文件还是文件夹
					getFileList(files[i].getAbsolutePath()); // 获取文件绝对路径
				} else if (fileName.endsWith("avi")) { // 判断文件名是否以.avi结尾
					String strFileName = files[i].getAbsolutePath();
					// System.out.println("---" + strFileName);
					filelist.add(strFileName);
				} else {
					continue;
				}
			}
		}
		return filelist;
	}

	public static SimpleDateFormat dateFormat() {
		return new SimpleDateFormat("MM-DD>HH:mm:ss");
	}

	public static SimpleDateFormat todayFormat() {
		return new SimpleDateFormat("YY-MM-DD");
	}

	public static String downloadWebImageToTmp(String URL, String fileName)
			throws IOException {
		URL url = new URL(URL);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(5 * 1000);
		InputStream is = con.getInputStream();
		byte[] bs = new byte[1024];
		int len;
		File sf = new File("C:\\\\imageTmp");
		String filenameString = System.currentTimeMillis() + "-" + fileName
				+ ".png";
		OutputStream os = new FileOutputStream(sf.getPath() + "\\"
				+ filenameString);
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		is.close();
		os.close();
		is = null;
		os = null;
		return filenameString;
	}

	public static String getWebFileMd5FuckCanotDeletLocalFile(
			InputStream inputStream, File file) throws IOException {
		MessageDigest digest = DigestUtils.getMd5Digest();
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(file);
			byte[] buffer = new byte[2048];
			int read = inputStream.read(buffer);
			while (read > -1) {
				digest.update(buffer, 0, read);
				outputStream.write(buffer, 0, read);
				read = inputStream.read(buffer);
			}
		} finally {
			IOUtils.closeQuietly(outputStream);
		}
		return Hex.encodeHexString(digest.digest());
	}

	public static String getWebImageMd5CodeSeeUdeathLocalFile(String Url,
			String fileName, boolean delete) throws IOException {
		String value = "";
		URL url = new URL(Url);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(5 * 1000);
		InputStream is = con.getInputStream();
		String filenameString = System.currentTimeMillis() + "-" + fileName
				+ ".png";
		File sf = new File("C:\\\\imageTmp" + "\\" + filenameString);
		value = getWebFileMd5FuckCanotDeletLocalFile(is, sf);
		is.close();
		is = null;
		if (delete) {
			sf.delete();
		}
		return value;
	}

	public static String getWebImageMd5(String Url, String fileName)
			throws IOException {
		String fileNameString = "";
		try {
			fileNameString = downloadWebImageToTmp(Url, fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		File file = new File("C:\\\\imageTmp\\" + fileNameString);
		FileInputStream in = new FileInputStream(file);
		String value = "";
		try {
			MappedByteBuffer byteBuffer = in.getChannel().map(
					FileChannel.MapMode.READ_ONLY, 0, file.length());
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(byteBuffer);
			BigInteger bi = new BigInteger(1, md5.digest());
			value = bi.toString(16);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
					in = null;
				}
			}
		}
		return value;
	}

	public static boolean downloadImageToTmp(String Url, String fileName) throws IOException {
		URL url = new URL(Url);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(5 * 1000);
		InputStream is = con.getInputStream();
		byte[] bs = new byte[1024];
		int len;
		File sf = new File("C:\\\\imageTmp");
		String filenameString =  fileName
				+ ".png";
		OutputStream os = new FileOutputStream(sf.getPath() + "\\"
				+ filenameString);
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		is.close();
		os.close();
		is = null;
		os = null;
		return false;
	}
	
	
	
	public static List<File> sortFiles(List<File> files) {
		List<File> toSortFiles = files;
		List<File> tmpList;
		List<File> resault = new ArrayList<File>();
		int minFileIndex = 0;
		for (int j = 0; j < files.size(); j++) {
			File minFile = toSortFiles.get(0);
			tmpList = new ArrayList<File>();
			for (int i = 0; i < toSortFiles.size(); i++) {
				if (Integer.valueOf(toSortFiles.get(i).getName()
						.replace(".jpg", "")) <= Integer.valueOf(minFile
						.getName().replace(".jpg", ""))) {
					minFile = toSortFiles.get(i);
					minFileIndex = i;
				}
			}
			for (int i = 0; i < toSortFiles.size(); i++) {
				if (i == minFileIndex) {
					continue;
				}
				tmpList.add(toSortFiles.get(i));
			}
			toSortFiles = tmpList;
			resault.add(minFile);
		}
//		for (int i = 0; i < resault.size(); i++) {
//			System.out.println(resault.get(i).getParent() + "\\" + (i + 1)
//					+ ".jpg");
//			resault.get(i).renameTo(
//					new File(resault.get(i).getParent() + "\\" + (i + 1)
//							+ ".jpg"));
//		}
		return resault;
	}

	public static Map<String, List<File>> getFileAllList(String strPath) {
		Map<String, List<File>> fileMap = new HashMap<String, List<File>>();

		File dir = new File(strPath);// 文件夹地址
		File[] files = dir.listFiles();// 文件夹下所有文件包括文件夹

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) {
					fileMap.putAll(getFileAllList(files[i].getAbsolutePath()));
				} else if (fileName.endsWith("jpg")) {
					fileMap.put(
							files[i].getParent(),
							addToList(fileMap.get(files[i].getParent()),
									files[i]));
				} else {
					continue;
				}
			}
		}
		return fileMap;
	}

	public static List<File> addToList(List<File> fileList, File file) {
		List<File> resault = new ArrayList<File>();
		if (fileList == null) {
			resault.add(file);
		} else {
			resault.addAll(fileList);
			resault.add(file);
		}
		return resault;
	}

}
