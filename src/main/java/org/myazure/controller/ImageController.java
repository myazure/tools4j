package org.myazure.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.myazure.api.ImageAPI;
import org.myazure.image.ImageImpl;
import org.myazure.utils.F;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ImageController {
	
	
	
	
	
	@Autowired
	public ImageController(){
		
	}
	
	
	
	
	
	private static final Logger LOG = LoggerFactory
			.getLogger(ImageController.class);

	@RequestMapping(path = "/image/ean/{imageAddress}", method = RequestMethod.GET)
	public void getWebImageEANCode(
			@PathVariable("imageAddress") String imageAddress,
			HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEE");
		
		String[] images = imageAddress.split("@");
		String eanCode = "";
		for (int i = 0; i < images.length; i++) {
			String resault = "";
			ImageAPI img = new ImageImpl();
			resault = img.getEAN13Code(images[i]);
			if (resault == "") {
				continue;
			} else {
				eanCode = resault;
				break;
			}
		}
			try {
				response.getWriter().write(eanCode);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	@RequestMapping(path = "/image/md5/{imageAddress}", method = RequestMethod.GET)
	public void getWebImageMd5(
			@PathVariable("imageAddress") String imageAddress,
			HttpServletRequest request, HttpServletResponse response) {
		int errCount = 0;
		String resaultString = "";
		try {
			resaultString = F.getWebImageMd5CodeSeeUdeathLocalFile(
					"http://www.yaofangwang.com/common/handler/imagetext.ashx?code="
							+ imageAddress, imageAddress, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		resaultString = getChineseString(resaultString);
		while ((resaultString == "空白" && errCount < 5)
				|| (resaultString == "" && errCount < 5)) {
			System.out.println(resaultString == "空白" ? "此地址获取到了空图片："
					+ imageAddress : "MD5哈希错误，未获取到hash值！");
			try {
				resaultString = F.getWebImageMd5CodeSeeUdeathLocalFile(
						"http://www.yaofangwang.com/common/handler/imagetext.ashx?code="
								+ imageAddress, imageAddress, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			resaultString = getChineseString(resaultString);
			if (errCount >= 5) {
				break;
			}
			errCount++;
			if (errCount == 5) {
				System.out.println("尝试了" + errCount + "次任然未获取到图片信息！");
			}
		}
		if (resaultString == "未知") {
			try {
				F.getWebImageMd5CodeSeeUdeathLocalFile(
						"http://www.yaofangwang.com/common/handler/imagetext.ashx?code="
								+ imageAddress, imageAddress, false);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			System.out.println("UNKNOW!!!!!         address" + imageAddress
					+ "   hash   " + resaultString.toLowerCase());
		}
		try {
			response.getWriter().write(resaultString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

	protected String getChineseString(String md5String) {
		String resaultString = "";
		switch (md5String.toLowerCase()) {
		case "f4e965a4bd39e2358231b35a9669232d":
			resaultString = "性状";
			break;
		case "ebedc4ba554eb1949fcd20d74ca9c739":
			resaultString = "性状";
			break;
		case "d0e747041b6a1d2aef4ac92cf1c96592":
			resaultString = "性状";
			break;
		case "61ef6ab5eaa64a8f49c44f3d4be9fdd":
			resaultString = "药理毒理";
			break;
		case "1de81395470e472fa5e7db1b5ca0af2e":
			resaultString = "药理毒理";
			break;
		case "6ebf94cdf3ed0c8ad6148c00289da907":
			resaultString = "不良反应";
			break;
		case "c523c8f5f0c7809a2838f6f0da68df2b":
			resaultString = "禁忌症";
			break;
		case "d43b211a8da5a798543d8b55f2a77d4d":
			resaultString = "注意事项";
			break;
		case "FF0CA410E88F26A46875514237F3D230":
			resaultString = "注意事项";
			break;
		case "74F6A0D84B55499774CB297162595876":
			resaultString = "执行标准";
			break;
		case "70cef215e83e00f2a90406ea96941fec":
			resaultString = "执行标准";
			break;
		case "86272b055ec7d7ce25a44de279fa2624":
			resaultString = "药代动力学";
			break;

		case "30f57c0194f05b144793be687171215a":
			resaultString = "儿童用药";
			break;
		case "3de4c211299713ac42d8716c1589cf27":
			resaultString = "儿童用药";
			break;
		case "a038666b4860d06d8d63edd249de7826":
			resaultString = "老年患者用药";
			break;
		case "7d363cc8d0365558212102490cd8187a":
			resaultString = "图片出错";
			break;
		case "60451054b8b79b79aee87a4988bb3a59":
			resaultString = "药物相互作用";
			break;
		case "149f33b0b5edffc82ad7bcf3d5c94c92":
			resaultString = "药物相互作用";
			break;
		case "fae9b41614d5b2d0a7b7bb88dbf63493":
			resaultString = "药物相互作用";
			break;
		case "45673887e7489e94ccc0b10ae3bb9a90":
			resaultString = "药物过量";
			break;
		case "72e1ca73078843d3b103eeffac602788":
			resaultString = "药物过量";
			break;
		case "fd3838ab0b9f8e7391d92046f9072d69":
			resaultString = "药物过量";
			break;
		case "ea87d73dc267b2120978a01fdb716556":
			resaultString = "药物过量";
			break;
		case "8f4f32302101805b652602231650d82d":
			resaultString = "药物过量";
			break;
		case "961a9fa28eeee24c0a765eabbded8504":
			resaultString = "作用类别";
			break;
		case "9bf4789dd8a6bbac5ef8f84220cd22f6":
			resaultString = "组方";
			break;
		case "822d381cf6076f87c51fd1f23319a37":
			resaultString = "主要原料";
			break;
		case "a41e060baa6d0ab0f72be8ef1d5aa127":
			resaultString = "禁忌症";
			break;
		case "0822d381cf6076f87c51fd1f23319a37":
			resaultString = "主要原料";
			break;

		case "ba9232d0e29859424668d300a32290b3":
			resaultString = "空白";
			break;
		default:
			resaultString = "未知";
			break;
		}
		return resaultString;
	}

}
