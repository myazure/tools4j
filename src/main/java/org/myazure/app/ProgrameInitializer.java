package org.myazure.app;

import javax.servlet.ServletContextEvent;

public interface ProgrameInitializer {
	
	public void weiXinTokenManagerContextInitialized(ServletContextEvent sce);

	public void weiXinTokenManagerContextDestroyed(ServletContextEvent sce);

	public void weiXinTicketManagerContextInitialized(ServletContextEvent sce);

	public void weiXinTicketManagerContextDestroyed(ServletContextEvent sce);
	
	
}
