package org.myazure.tools;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileNameRenamer {
	public static void main(String[] args) {
		String strPath = "H:\\images";

		Map<String, List<File>> files = getFileAllList(strPath);

		for (String string : files.keySet()) {
			System.out.println("----" + string);
			List<File> resault = sortFiles(files.get(string));
			for (File file : resault) {
				System.out.println("-------" + file.getName());
			}

		}

	}

	public static List<File> sortFiles(List<File> files) {
		List<File> toSortFiles = files;
		List<File> tmpList;
		List<File> resault = new ArrayList<File>();
		int minFileIndex = 0;
		for (int j = 0; j < files.size(); j++) {
			File minFile = toSortFiles.get(0);
			tmpList = new ArrayList<File>();
			for (int i = 0; i < toSortFiles.size(); i++) {
				if (Integer.valueOf(toSortFiles.get(i).getName()
						.replace(".jpg", "")) <= Integer.valueOf(minFile
						.getName().replace(".jpg", ""))) {
					minFile = toSortFiles.get(i);
					minFileIndex = i;
				}
			}
			for (int i = 0; i < toSortFiles.size(); i++) {
				if (i == minFileIndex) {
					continue;
				}
				tmpList.add(toSortFiles.get(i));
			}
			toSortFiles = tmpList;
			resault.add(minFile);
		}
		for (int i = 0; i < resault.size(); i++) {
			System.out.println(resault.get(i).getParent() + "\\" + (i + 1)
					+ ".jpg");
			resault.get(i).renameTo(
					new File(resault.get(i).getParent() + "\\" + (i + 1)
							+ ".jpg"));
		}
		return resault;
	}

	public static Map<String, List<File>> getFileAllList(String strPath) {
		Map<String, List<File>> fileMap = new HashMap<String, List<File>>();

		File dir = new File(strPath);// 文件夹地址
		File[] files = dir.listFiles();// 文件夹下所有文件包括文件夹

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) {
					fileMap.putAll(getFileAllList(files[i].getAbsolutePath()));
				} else if (fileName.endsWith("jpg")) {
					fileMap.put(
							files[i].getParent(),
							addToList(fileMap.get(files[i].getParent()),
									files[i]));
				} else {
					continue;
				}
			}
		}
		return fileMap;
	}

	public static List<File> addToList(List<File> fileList, File file) {
		List<File> resault = new ArrayList<File>();
		if (fileList == null) {
			resault.add(file);
		} else {
			resault.addAll(fileList);
			resault.add(file);
		}
		return resault;
	}

}
