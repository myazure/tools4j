package org.myazure.tools;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.myazure.api.ImageAPI;
import org.myazure.image.ImageImpl;
import org.myazure.utils.F;

public class FindEANCode {
	public static void main(String[] args) {
		String strPath = "H:\\images";
		int num=0;
		int allCount=0;
		Map<String, List<File>> files = F.getFileAllList(strPath);
		for (String string : files.keySet()) {
//			System.out.println("----" + string);
			allCount++;
			List<File> resault = F.sortFiles(files.get(string));
			for (File file : resault) {
//				System.out.println("-------" + file.getName());
				 ImageAPI img=new ImageImpl();
				 String eanCodeString="";
				 try {
					 eanCodeString=img.getEAN13Code(file);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("ERR:"+file.getName());
					System.err.println("ERR:"+file.getAbsolutePath());
					continue;
				}
				if (eanCodeString=="") {
					
				}else {
//					System.out.println("+++++++"+eanCodeString);
					num++;
					System.out.println("num:"+num+" All:"+allCount);
				}
			}

		}
		System.out.println("EAN Counts = " +num);
		System.out.println("All Counts = " +allCount);
		
	}
}
